<!DOCTYPE html>
<html>
<head> 
	
@foreach ($map as $d)

<input type="hidden" id='long1' value='{{$d->longitut}}'>
<input type="hidden" id='lat1' value='{{$d->latitut}}'>

<input type="hidden" id='status' value='{{$d->status}}'>

@endforeach
	<title>Quick Start - Leaflet</title>

	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<link rel="shortcut icon" type="image/x-icon" href="docs/images/favicon.ico" />

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>


	
</head>
<body>


<a href="/python"><button class='btn btn-primary'>Refresh</button></a>

<div id="mapid" style="width: 600px; height: 400px;"></div>
<script>

    var mymap = L.map('mapid').setView([document.getElementById('lat1').value, document.getElementById('long1').value], 13);
    console.log(document.getElementById('long1').value);
    console.log(document.getElementById('lat1').value);
	var database = [[document.getElementById('lat1').value,document.getElementById('long1').value,document.getElementById('status').value]];
	


	L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
			'Imagery ©️ <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox/streets-v11',
		tileSize: 512,
		zoomOffset: -1
	}).addTo(mymap);
	// var status = 'Berlari';
	// var longlat = [51.5, -0.09];

	for (let index = 0; index < database.length; index++) {

		L.marker([database[index][0],database[index][1]]).addTo(mymap)
		.bindPopup(database[index][2]).openPopup();
	
	}

	var popup = L.popup();

	function onMapClick(e) {
		popup
			.setLatLng(e.latlng)
			.setContent("You clicked the map at " + e.latlng.toString())
			.openOn(mymap);
	}

	mymap.on('click', onMapClick);

</script>



</body>
</html>