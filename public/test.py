import os
from pathlib import Path

import pickle
import pandas as pd
import numpy as np
loaded_model = pickle.load(open("SVM.sav", 'rb'))
fin = open("data_file/Final_Rekap.csv","r")
data = fin.read()
data = data.replace('"','')
fin.close()

fin = open('data_file/Final_Rekap.csv',"w")
fin.write(data)
fin.close()

df = pd.read_csv('data_file/Final_Rekap.csv')
FRAME_LENGTH=999
x_acc_cols = [f'acc_x_{i}' for i in range(FRAME_LENGTH)]
y_acc_cols = [f'acc_y_{i}' for i in range(FRAME_LENGTH)]
z_acc_cols = [f'acc_z_{i}' for i in range(FRAME_LENGTH)]

frames = []
labels = []

lat = df['Latitude'][len(df['Latitude'])-1]
long = df['Longitude'][len(df['Longitude'])-1]

flat_frame = np.hstack([df['accelerometer_X'], df['accelerometer_Y'], df['accelerometer_Z']]).astype(float)        
frames.append(flat_frame)
# # labels.append(class_name)
raw_data = pd.DataFrame(frames, columns = x_acc_cols+y_acc_cols+z_acc_cols)
# raw_data = pd.concat([raw_data, pd.Series(labels, name='label')], axis=1)
# raw_data.head(3)
X = raw_data[x_acc_cols+y_acc_cols+z_acc_cols]
nama=loaded_model.predict(X)[0]
print(nama+';'+str(lat)+';'+str(long))
# Path(nama).touch()